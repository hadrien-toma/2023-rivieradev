# 2023-rivieradev

## Targeted dependency graph

```mermaid
flowchart TD
    A["json/q-format/back/app/contract"] --> B["🐱 json/q-format/back/app/server"]
    C["🅰️ json/q-format/back/app/client"] --> A
    C --> D["🅰️ json/q-format/back/routes/route/client"]
    B --> E["🐱 json/q-format/back/routes/route/server"]
    F["🅰️ json/q-format/back/routes/request/validate/client"] --> G["json/q-format/back/routes/request/validate/contract"]
    G --> H["🐱 json/q-format/back/routes/request/validate/server"]
    E --> I["📜 json/q-format/ts"]
    E --> H
    J["json/q-format/back/routes/route/contract"] --> E
    H --> K["📜 json/q-validate/ts"]
    D --> K
    L["🐱 json/q-format/front/routes/route/server"] --> K
    M["🅰️ json/q-format/front/routes/route/client"] --> K
    L --> I
    D --> J
    N["📟 json/q-format/cli"] --> I
    O["🎨 json/q-format/front/uis/formatter/ui"] --> P["🎨 json/q-format/front/uis/formatter/uis/query/ui"]
    O --> Q["🎨 json/q-format/front/uis/formatter/uis/results/ui"]
    P --> R["🎨 front/uis/textarea/ui"]
    Q --> R
    S["json/q-format/front/app/contract"] --> T["🅰️ json/q-format/front/app/client"]
    U["🐱 json/q-format/front/app/server"] --> S
    U --> L
    T --> M
    M --> V["🪈 json/q-format/front/pipes/main"]
    V --> W["💁 json/q-format/front/services/main"]
    X["json/q-format/front/routes/route/contract"] --> M
    M --> O
    L --> X
    W --> I
    Y["📟 json/q-validate/cli"] --> K
    Z["📜 text/q-format/ts"] --> I
```
