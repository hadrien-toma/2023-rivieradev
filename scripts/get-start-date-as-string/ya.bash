#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)
cd "${repoDir}"
rc=0

startDateAsS=${START_DATE_AS_S:-$(date +%s)}

echo "${startDateAsS}"

exit ${rc}
