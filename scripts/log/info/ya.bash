#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)

#region args
startDateAsS=${1}
lineNumber=${2}
processPid=${3}
sourceScript=${4}

valuePartList=${5}
valuePartEscapedList=`echo ${valuePartList} | sed 's@%@%%@g'`
#endregion

ellapsedSeconds() {
	localStartDateAsS=${1}

	seconds="$(($(date +%s) - ${localStartDateAsS}))"
	startDateAsS="${localStartDateAsS}"

	hoursPadded=`printf "%02d\n" $(( ${seconds} / 3600 ))`
	minutesPadded=`printf "%02d\n" $(( (${seconds} / 60) % 60 ))`
	secondsPadded=`printf "%02d\n" $(( ${seconds} % 60 ))`

	echo "${hoursPadded}:${minutesPadded}:${secondsPadded}"
}

source=`echo ${sourceScript} | sed 's/\.\./ /g' | rev | cut -d' ' -f1 | rev | sed 's@^\./@@g'`":${lineNumber}"
repoDirLength=$(echo "${repoDir}" | wc --char)
relativeSource=$(echo "${source}" | sed "s~${repoDir}/~~g" | sed "s~${repoDir}~~g")

maxValuePartEscapedKeyLength="0"
OIFS="${IFS}"
IFS=' '
for valuePartEscaped in ${valuePartEscapedList} ; do
	valuePartEscaped=`echo ${valuePartEscaped} `
	valuePartEscapedKey=`printf ${valuePartEscaped} | cut -d'=' -f1`
	if [ ${#valuePartEscapedKey} -ge ${maxValuePartEscapedKeyLength} ] ; then
		maxValuePartEscapedKeyLength="${#valuePartEscapedKey}"
	fi
done
for valuePartEscaped in ${valuePartEscapedList} ; do
	valuePartEscapedKey=`printf ${valuePartEscaped} | cut -d'=' -f1`
	valuePartEscapedKeyPadded=`printf "%-${maxValuePartEscapedKeyLength}s\n" ${valuePartEscapedKey}`
	valuePartEscapedValue=`printf ${valuePartEscaped} | cut -d'=' -f2`
	ellapsedSeconds=$(ellapsedSeconds "${startDateAsS}")
	echo "ℹ️  [${ellapsedSeconds}][${relativeSource}] (${processPid}->$PPID) · ${valuePartEscapedKeyPadded} ${valuePartEscapedValue}"
done
IFS="${OIFS}"
