#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}

"${repoDir}/scripts/seconds-to-human/ya.bash" \
	"$(($(date +%s) - ${startDateAsS}))" \
	"${startDateAsS}"
