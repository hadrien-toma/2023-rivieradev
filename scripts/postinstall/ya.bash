#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)
pidList=""
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-$("${repoDir}/scripts/git/get/current-branch-name/ya.bash" "${startDateAsS}")}
isFromCi=${3:-${IS_FROM_CI:-"false"}}

logEnter="${repoDir}/scripts/log/enter/ya.bash"
logInfo="${repoDir}/scripts/log/info/ya.bash"
logExit="${repoDir}/scripts/log/exit/ya.bash"

"${logEnter}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" \ "
	branchName=${branchName}
	isFromCi=${isFromCi}
"

cd "${repoDir}"

"${repoDir}/scripts/vscode/install-recommended-extensions/ya.bash" "${startDateAsS}" "${branchName}" "${isFromCi}"

for pidItem in ${pidList}; do
	wait ${pidItem} || let "rc=1"
done

cd "${repoDir}"

"${logInfo}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" \ "
	rc=${rc}
"

"${logExit}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}"

exit ${rc}
