#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)

seconds=${1}
startDateAsS=${2}

hoursPadded=`printf "%02d\n" $(( ${seconds} / 3600 ))`
minutesPadded=`printf "%02d\n" $(( (${seconds} / 60) % 60 ))`
secondsPadded=`printf "%02d\n" $(( ${seconds} % 60 ))`

echo "${hoursPadded}:${minutesPadded}:${secondsPadded}"
