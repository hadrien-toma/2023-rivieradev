#!/usr/bin/env bash

set -eE

clean() { if [ "${pidList}X" != "X" ] ; then for pid in ${pidList} ; do kill -9 ${pid}; done; fi; exit 2; }

trap 'clean' SIGINT SIGTERM

repoDir=$(git rev-parse --show-toplevel)
pidList=""
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-$("${repoDir}/scripts/git/get/current-branch-name/ya.bash" "${startDateAsS}")}
isFromCi=${3:-${IS_FROM_CI:-"false"}}
firstFilePath=${4:-${FIRST_FILE_PATH:-"${repoDir}/docs/chat-gpt/test3.5.txt"}}
secondFilePath=${5:-${SECOND_FILE_PATH:-"${repoDir}/docs/chat-gpt/test4.0.txt"}}
cliOptions=${@:6}

logEnter="${repoDir}/scripts/log/enter/ya.bash"
logInfo="${repoDir}/scripts/log/info/ya.bash"
logExit="${repoDir}/scripts/log/exit/ya.bash"

"${logEnter}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" \ "
	branchName=${branchName}
	isFromCi=${isFromCi}
	cliOptions=${cliOptions}
"

cd "${repoDir}"

GIT_PAGER=cat git diff --no-index --unified=9999999999 "${firstFilePath}" "${secondFilePath}"

for pidItem in ${pidList}; do
	wait ${pidItem} || let "rc=1"
done

"${logInfo}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" \ "
	rc=${rc}
"

"${logExit}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}"

exit ${rc}
