#!/usr/bin/env bash

set -eE

clean() { if [ "${pidList}X" != "X" ] ; then for pid in ${pidList} ; do kill -9 ${pid}; done; fi; exit 2; }

trap 'clean' SIGINT SIGTERM

repoDir=$(git rev-parse --show-toplevel)
pidList=""
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-$("${repoDir}/scripts/git/get/current-branch-name/ya.bash" "${startDateAsS}")}
isFromCi=${3:-${IS_FROM_CI:-"false"}}
rootPath=${4:-${ROOT_PATH:-"${repoDir}"}}
secondFilePath=${5:-${SECOND_FILE_PATH:-"${repoDir}/TODO.md"}}
cliOptions=${@:6}

logEnter="${repoDir}/scripts/log/enter/ya.bash"
logInfo="${repoDir}/scripts/log/info/ya.bash"
logExit="${repoDir}/scripts/log/exit/ya.bash"

"${logEnter}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" \ "
	branchName=${branchName}
	isFromCi=${isFromCi}
	cliOptions=${cliOptions}
"

cd "${repoDir}"

find "apps/kaliz" -type f | while read -r file; do
	if git ls-files -u | grep -q "${file}\$"; then
		git checkout --theirs -- "${file}"
		git add "${file}"
	fi
done

for pidItem in ${pidList}; do
	wait ${pidItem} || let "rc=1"
done

"${logInfo}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" \ "
	rc=${rc}
"

"${logExit}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}"

exit ${rc}
