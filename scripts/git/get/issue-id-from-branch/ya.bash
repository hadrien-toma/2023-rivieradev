#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)
cd "${repoDir}"
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-$("${repoDir}/scripts/git/get/current-branch-name/ya.bash" "${startDateAsS}")}

issueId=$(echo "${branchName}" | cut -d'_' -f1)

issueIdIsNumeric=$("${repoDir}/scripts/bash/is-variable-numeric/ya.bash" "${startDateAsS}" "${issueId}")

if [ "${issueIdIsNumeric}" = "false" ] ; then
	issueId=""
	rc=1
fi

echo "${issueId}"

exit ${rc}
