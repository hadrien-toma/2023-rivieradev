#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)
cd "${repoDir}"
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}

branchName=${BRANCH_NAME:-$(git branch -a | grep "^* " | cut -d' ' -f2)}

echo "${branchName}"

exit ${rc}
