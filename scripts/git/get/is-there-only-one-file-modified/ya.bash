#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)
cd "${repoDir}"
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-${BRANCH_NAME:-$(git branch -a | grep "^* " | cut -d' ' -f2)}}

outputs=""
gitStatusPorcelainGrepCountResponse=$(git status --porcelain | grep --count "^ M")

if [ "${gitStatusPorcelainGrepCountResponse}X" = "1X" ] ; then
  outputs="yes";
else
  outputs="no";
fi

echo "${outputs}"

exit ${rc}
