#!/usr/bin/env bash

repoDir=$(git rev-parse --show-toplevel)
cd "${repoDir}"
rc=0

startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-${BRANCH_NAME:-$(git branch -a | grep "^* " | cut -d' ' -f2)}}

outputs=""
gitPushDryRunPorcelainResponse=$(git push --dry-run --porcelain 2>/dev/null | grep "\[up to date\]")

if [[ "${gitPushDryRunPorcelainResponse}" =~ "[up to date]" ]] ; then
	outputs="yes";
else
	outputs="no";
fi

echo "${outputs}"

exit ${rc}
