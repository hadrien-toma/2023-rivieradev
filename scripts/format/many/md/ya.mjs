import prettier from 'prettier';
export const format = ({ content }) => {
	const formattedContent = prettier.format(content, {
		parser: 'markdown',
		tabWidth: 1,
		useTabs: false,
		proseWrap: 'never',
	});
	return formattedContent.replace(/\n\s*\n\s*\n/g, '\n\n');
};
