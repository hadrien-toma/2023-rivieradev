export function formatReplacer(_key, value) {
	return value instanceof Object && !Array.isArray(value)
		? Object.keys(value)
				.sort()
				.reduce((sorted, key) => {
					sorted[key] = value[key];
					return sorted;
				}, {})
		: value;
}

export async function format({ content, path }) {
	const hasCommentAtFirstLine = content.startsWith('/* ');
	const cleanedContent = hasCommentAtFirstLine ? content.split(' */').slice(1).join() : content;
	const cleanedContentAsJson = JSON.parse(cleanedContent);
	const cleanedContentAsJsonWithSortedKeysStringified = JSON.stringify(cleanedContentAsJson, formatReplacer, '\t');
	return cleanedContentAsJsonWithSortedKeysStringified;
}
