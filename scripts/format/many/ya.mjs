import { readFileSync, writeFileSync } from 'fs';
import { format as babelrc } from './babelrc/ya.mjs';
import { format as bash } from './bash/ya.mjs';
import { format as css } from './css/ya.mjs';
import { format as html } from './html/ya.mjs';
import { format as js } from './js/ya.mjs';
import { format as json } from './json/ya.mjs';
import { format as md } from './md/ya.mjs';
import { format as mjs } from './mjs/ya.mjs';
import { format as scss } from './scss/ya.mjs';
import { format as svg } from './svg/ya.mjs';
import { format as swcrc } from './swcrc/ya.mjs';
import { format as ts } from './ts/ya.mjs';
import { format as xml } from './xml/ya.mjs';
import { format as yml } from './yml/ya.mjs';

const formatters = {
	babelrc,
	bash,
	css,
	html,
	js,
	json,
	md,
	mjs,
	scss,
	swcrc,
	svg,
	ts,
	xml,
	yml,
};

export function format({ relativeOrAbsoluteFilePathList }) {
	const processCurrentWorkingDirectory = process.cwd();
	const npmScope = JSON.parse(readFileSync(`${processCurrentWorkingDirectory}/package.json`, 'utf-8')).name;
	relativeOrAbsoluteFilePathList.forEach(async (absoluteOrRelativeFilePath) => {
		const isFilePathAbsolute = absoluteOrRelativeFilePath.startsWith('/');

		let filePath = absoluteOrRelativeFilePath;
		if (!isFilePathAbsolute) {
			filePath = `${processCurrentWorkingDirectory}/${absoluteOrRelativeFilePath}`;
		}

		const fileHasExtension = filePath.includes('.');
		if (fileHasExtension) {
			const filePathSgList = filePath.split('.');
			const isTemplate = filePathSgList[filePathSgList.length - 1].endsWith('__tp__');
			const fileExtension = filePathSgList[filePathSgList.length - 1].replace('__tp__', '');
			const hasFileExtensionAFormatter = Object.keys(formatters).includes(fileExtension);

			if (hasFileExtensionAFormatter) {
				try {
					const content = readFileSync(filePath, 'utf-8');
					const formattedContent = await formatters[fileExtension]({ content, filePath: filePath, npmScope });
					const formattedContentIsNotEmpty = formattedContent.length > 0;
					const contentWasNotAlreadyFormatted = content !== formattedContent;
					const fileNeedToBeWritten = formattedContentIsNotEmpty && contentWasNotAlreadyFormatted;

					if (fileNeedToBeWritten) {
						// console.log(`Formatting file: ${filePath}...`);
						try {
							writeFileSync(filePath, formattedContent);
							console.log(`Formatted file: ${filePath}`);
						} catch (error) {
							if (!isTemplate) {
								console.error({ filePath, error });
							}
							writeFileSync(filePath, content);
						}
					}
				} catch (error) {
					if (!isTemplate) {
						console.error({ filePath, error });
					}
				}
			}
		}
	});
}

function cli() {
	const isCommandCalledFromCli = process.argv.length > 2;

	if (isCommandCalledFromCli) {
		const relativeOrAbsoluteFilePathList = process.argv[2].split(' ');

		format({
			relativeOrAbsoluteFilePathList,
		});
	}
}

cli();
