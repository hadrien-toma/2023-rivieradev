import prettier from 'prettier';
export async function format({ content, filePath }) {
	const formattedContent = prettier.format(content, {
		parser: 'angular',
		printWidth: 150,
		tabWidth: 2,
		useTabs: true,
		htmlWhitespaceSensitivity: 'strict',
		attributeGroups: [
			'$ANGULAR_STRUCTURAL_DIRECTIVE',
			'^id$',
			'^\\[attr.id\\]$',
			'^name$',
			'^class$',
			'$DEFAULT',
			'^data-',
			'^aria-',
			'$ANGULAR_INPUT',
			'$ANGULAR_TWO_WAY_BINDING',
			'$ANGULAR_OUTPUT',
		],
		attributeSort: 'ASC',
	});
	return formattedContent.replace(/\n\s*\n\s*\n/g, '\n\n');
}
