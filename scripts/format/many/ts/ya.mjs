import { EOL } from 'os';
import prettier from 'prettier';

export function identifyPrintWidth({ filePath }) {
	switch (true) {
		case filePath.includes('actions.'):
			return 150;
		case filePath.includes('actions.types'):
			return 150;
		case filePath.includes('component.'):
			return 150;
		case filePath.includes('cfgs.'):
			return 150;
		case filePath.includes('effects.'):
			return 150;
		case filePath.includes('interceptor.'):
			return 150;
		case filePath.includes('module.'):
			return 150;
		case filePath.includes('reducer.'):
			return 150;
		case filePath.includes('selectors.'):
			return 150;
		default:
			return 150;
	}
}

export function organizeImports({ content, npmScope } = { content: '', npmScope: '' }) {
	if (content.includes('import' + ' ')) {
		const isContentStartingWithEslintIgnore = content.startsWith('/* eslint-disable */\n');
		const withoutFromImportArray = [];
		//use + to avoid matchers reserved for imports
		const contentSplittedByImport = content.split('import' + ' ');
		const contentSplittedByImportLastItem = contentSplittedByImport.slice(-1)[0];
		const hasContentSplittedByImportLastItemFrom = contentSplittedByImportLastItem.includes(' ' + 'from' + ' ');
		const contentSplittedByImportLastItemSplittedByFrom = contentSplittedByImportLastItem.split(' ' + 'from' + ' ');
		let lastImportStatement = undefined;
		let contentSplittedByImportLastItemSplittedByFromLastItemSplittedByEol = undefined;
		if (!hasContentSplittedByImportLastItemFrom) {
			withoutFromImportArray.push(`import` + ` ` + `${contentSplittedByImportLastItemSplittedByFrom[0]}`);
			const contentSplittedByImportLastItemSplittedByEol = contentSplittedByImportLastItem.split(`;`)[1];
			contentSplittedByImportLastItemSplittedByFromLastItemSplittedByEol = contentSplittedByImportLastItemSplittedByEol.split(EOL);
		} else {
			contentSplittedByImportLastItemSplittedByFromLastItemSplittedByEol = contentSplittedByImportLastItemSplittedByFrom[1].split(EOL);
			const hasLastItemCurlyBrackets = contentSplittedByImportLastItemSplittedByFrom[0].includes('}');
			lastImportStatement = {
				hasCurlyBrackets: hasLastItemCurlyBrackets,
				imports: (hasLastItemCurlyBrackets
					? contentSplittedByImportLastItemSplittedByFrom[0].split('{')[1].split('}')[0]
					: contentSplittedByImportLastItemSplittedByFrom[0]
				)
					.split(',')
					.filter((importItem) => importItem !== EOL && importItem !== '\n')
					.map((importItem) => importItem.trim())
					.sort((importItemA, importItemB) => {
						const isImportItemAStartingByLowercase = importItemA.startsWith(importItemA[0].toLowerCase());
						const isImportItemBStartingByLowercase = importItemB.startsWith(importItemB[0].toLowerCase());
						switch (true) {
							case isImportItemAStartingByLowercase && !isImportItemBStartingByLowercase:
								return -1;
							case !isImportItemAStartingByLowercase && isImportItemBStartingByLowercase:
								return 1;
							default:
								return importItemA > importItemB ? 1 : importItemA < importItemB ? -1 : 0;
						}
					})
					.join(`,${EOL}`),
				source: contentSplittedByImportLastItemSplittedByFromLastItemSplittedByEol[0],
			};
		}
		const contentWithoutImports = contentSplittedByImportLastItemSplittedByFromLastItemSplittedByEol.slice(1).join(EOL);
		const organizedImports = [
			...contentSplittedByImport
				.slice(0, -1) // -1: last item is treated before and contains the content without import
				.filter((importItem) => importItem !== '' && importItem !== '/* eslint-disable */\n' && !importItem.startsWith('//'))
				.map((importItem) => {
					const hasImportItemFrom = importItem.includes(' ' + 'from' + ' ');
					const importItemSplittedByFrom = importItem.split(' ' + 'from' + ' ');
					if (!hasImportItemFrom) {
						withoutFromImportArray.push(`import` + ` ` + `${importItemSplittedByFrom[0]}`);
						return null;
					} else {
						const hasCurlyBrackets = importItemSplittedByFrom[0].includes('}');
						const imports = (hasCurlyBrackets ? importItemSplittedByFrom[0].split('{')[1].split('}')[0] : importItemSplittedByFrom[0])
							.split(',')
							.filter((importItem) => importItem !== EOL && importItem !== '\n')
							.map((importItem) => importItem.trim())
							.sort((importItemA, importItemB) => {
								const isImportItemAStartingByLowercase = importItemA.startsWith(importItemA[0].toLowerCase());
								const isImportItemBStartingByLowercase = importItemB.startsWith(importItemB[0].toLowerCase());
								switch (true) {
									case isImportItemAStartingByLowercase && !isImportItemBStartingByLowercase:
										return -1;
									case !isImportItemAStartingByLowercase && isImportItemBStartingByLowercase:
										return 1;
									default:
										return importItemA > importItemB ? 1 : importItemA < importItemB ? -1 : 0;
								}
							})
							.join(`,${EOL}`);
						const source = importItemSplittedByFrom[1].split(EOL)[0];
						return {
							hasCurlyBrackets,
							imports,
							source,
						};
					}
				})
				.flat()
				.filter((importStatement) => importStatement !== null),
			...(lastImportStatement === undefined ? [] : [lastImportStatement]),
		]
			.sort((importStatementA, importStatementB) => {
				const isRelativeImportA = importStatementA.source.startsWith("'./") || importStatementA.source.startsWith("'../");
				const isRelativeImportB = importStatementB.source.startsWith("'./") || importStatementB.source.startsWith("'../");
				const isWorkspaceImportA =
					importStatementA.source.startsWith(`'@${npmScope}`) || importStatementA.source.startsWith(`'@<%= npmScope %>`);
				const isWorkspaceImportB =
					importStatementB.source.startsWith(`'@${npmScope}`) || importStatementB.source.startsWith(`'@<%= npmScope %>`);
				switch (true) {
					case isRelativeImportA && !isRelativeImportB:
						return 1;
					case !isRelativeImportA && isRelativeImportB:
						return -1;
					case !isWorkspaceImportA && isWorkspaceImportB:
						return -1;
					case isWorkspaceImportA && !isWorkspaceImportB:
						return 1;
					default:
						return importStatementA.source > importStatementB.source ? 1 : importStatementA.source < importStatementB.source ? -1 : 0;
				}
			})
			.map(({ hasCurlyBrackets, imports, source }) => imports.split(`,${EOL}`).map((importItem) => ({ hasCurlyBrackets, importItem, source })))
			.flat()
			.reduce((localImportStatementArray, { hasCurlyBrackets, importItem, source }) => {
				const indexOfItemSource = localImportStatementArray.map((localImportStatement) => localImportStatement.source).indexOf(source);
				const wasItemSourceAlreadyEncountered = indexOfItemSource > -1;
				if (wasItemSourceAlreadyEncountered) {
					const indexOfImportItem = localImportStatementArray[indexOfItemSource].imports.indexOf(importItem);
					const wasImportItemAlreadyEncountered = indexOfImportItem > -1;
					if (!wasImportItemAlreadyEncountered) {
						localImportStatementArray[indexOfItemSource].imports = [...localImportStatementArray[indexOfItemSource].imports, importItem];
					}
				} else {
					localImportStatementArray.push({ hasCurlyBrackets, imports: [importItem], source });
				}
				return localImportStatementArray;
			}, [])
			.map(
				({ hasCurlyBrackets, imports, source }) =>
					'import' + ` ${hasCurlyBrackets ? '{' : ''} ${imports}${hasCurlyBrackets ? '}' : ''} ` + 'from' + ` ${source}`,
			)
			.join(EOL);
		return `${isContentStartingWithEslintIgnore ? '/* eslint-disable */\n' : ''}${organizedImports}${EOL}${withoutFromImportArray.join(
			EOL,
		)}${contentWithoutImports}`;
	} else {
		return content;
	}
}

export function format({ content, filePath, npmScope }) {
	if (!filePath.includes('projects.bu') && !filePath.includes('projects.err') && !filePath.includes('vite.config.ts')) {
		const printWidth = identifyPrintWidth({ filePath });
		const isOrganizeImportTriggered = !filePath.endsWith('test-setup.ts') && !filePath.endsWith('test-setup.ts__tp__');
		const formattedContent = prettier.format(isOrganizeImportTriggered ? organizeImports({ content, npmScope }) : content, {
			arrowParens: 'always',
			bracketSpacing: true,
			parser: 'typescript',
			printWidth,
			semi: true,
			singleQuote: true,
			tabWidth: 4,
			trailingComma: 'all',
			useTabs: true,
		});
		return formattedContent;
	} else {
		return content;
	}
}
