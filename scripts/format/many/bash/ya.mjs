export const format = ({ content }) => {
	return content.replace(/\n\s*\n\s*\n/g, '\n\n');
};
