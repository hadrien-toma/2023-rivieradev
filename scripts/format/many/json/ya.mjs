export function formatReplacer(_key, value) {
	return value instanceof Object && !Array.isArray(value)
		? Object.keys(value)
				.sort()
				.reduce((sorted, key) => {
					sorted[key] = value[key];
					return sorted;
				}, {})
		: value;
}

export async function format({ content, path }) {
	const hasCommentAtFirstLine = content.startsWith('/* ');
	const cleanedContent = hasCommentAtFirstLine ? content.split(' */').slice(1).join() : content;
	const cleanedContentAsJson = JSON.parse(cleanedContent);
	const cleanedContentAsJsonWithSortedKeysStringified = JSON.stringify(cleanedContentAsJson, formatReplacer, '\t');
	// const cleanedContentAsJsonWithSortedKeysStringifiedWithUnicodeReplacement = cleanedContentAsJsonWithSortedKeysStringified;
	const cleanedContentAsJsonWithSortedKeysStringifiedWithUnicodeReplacement = cleanedContentAsJsonWithSortedKeysStringified.replace(
		/[\u2000-\uFFFF]/g,
		function (character) {
			return '\\u' + ('0000' + character.charCodeAt(0).toString(16)).slice(-4);
		},
	);
	return cleanedContentAsJsonWithSortedKeysStringifiedWithUnicodeReplacement;
}
