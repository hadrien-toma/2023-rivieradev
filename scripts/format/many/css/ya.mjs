import prettier from 'prettier';

export async function format({ content, filePath }) {
	return prettier
		.format(content, {
			parser: 'css',
			printWidth: 150,
			tabWidth: 4,
			useTabs: true,
		})
		.replace(/\n\s*\n\s*\n/g, '\n\n');
}
