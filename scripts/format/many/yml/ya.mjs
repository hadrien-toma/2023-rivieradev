import prettier from 'prettier';

export async function format({ content, filePath }) {
	const formattedContent = prettier.format(content, {
		parser: 'yaml',
		printWidth: 150,
		tabWidth: 4,
		useTabs: false,
	});
	return formattedContent;
}
