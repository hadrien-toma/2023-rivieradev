import prettier from 'prettier';
export async function format({ content, filePath }) {
	const formattedContent = prettier.format(content, {
		parser: 'html',
		printWidth: 150,
		tabWidth: 2,
		useTabs: true,
		htmlWhitespaceSensitivity: 'strict',
		attributeGroups: ['^id$', '^\\[attr.id\\]$', '^name$', '^class$', '$DEFAULT', '^data-', '^aria-'],
		attributeSort: 'ASC',
	});
	return formattedContent.replace(/\n\s*\n\s*\n/g, '\n\n');
}
