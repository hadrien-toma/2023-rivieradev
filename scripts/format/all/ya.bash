#!/usr/bin/env bash

# Set -eE to exit immediately if any command fails and to capture errors in functions and subshells
set -eE

# First, let's create a clean up function to kill pids and exit with 2 if SIGINT or SIGTERM is received
clean() { if [ "${pidList}X" != "X" ] ; then for pid in ${pidList} ; do kill -9 ${pid}; done; fi; exit 2; }

# Then, let's register the clean function to be called when SIGINT or SIGTERM is received
trap 'clean' SIGINT SIGTERM

# Next, let's get the path of the repository and initialize the list of pids and the result code
repoDir=$(git rev-parse --show-toplevel)
pidList=""
rc=0

# Now, let's initialize variables with values provided as command-line arguments
startDateAsS=${1:-$("${repoDir}/scripts/get-start-date-as-string/ya.bash")}
branchName=${2:-$("${repoDir}/scripts/git/get/current-branch-name/ya.bash" "${startDateAsS}")}
isFromCi=${3:-${IS_FROM_CI:-"false"}}
cliOptions=${@:4}

# Afterward, let's set logs variables to the paths of their corresponding scripts
logEnter="${repoDir}/scripts/log/enter/ya.bash"
logInfo="${repoDir}/scripts/log/info/ya.bash"
logExit="${repoDir}/scripts/log/exit/ya.bash"

# Subsequently, let's log the command-line arguments
"${logEnter}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" "branchName=${branchName} isFromCi=${isFromCi} cliOptions=${cliOptions}"

# Initially, let's change current directory to the repository directory
cd "${repoDir}"

# Furthermore, let's format all files in the repository
node "${repoDir}/scripts/format/all/ya.mjs" "${repoDir}"

# Additionally, let's wait for all pids and set rc to 1 if any exits with non-zero
for pidItem in ${pidList}; do wait ${pidItem} || let "rc=1"; done

# In conclusion, let's log the exit status
"${logInfo}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}" "rc=${rc}"

# Finally, let's log the end
"${logExit}" "${startDateAsS}" "${LINENO}" "${PPID}" "${0}"

# Ultimately, let's exit with the final result code
exit ${rc}
