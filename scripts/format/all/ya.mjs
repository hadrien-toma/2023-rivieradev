import { watch as listen } from 'chokidar';
import { format } from '../../../scripts/format/many/ya.mjs';

export function getIgnoredPathList({ repoDir }) {
	return [
		`${repoDir}/**/.angular`,
		`${repoDir}/**/.git`,
		`${repoDir}/**/.nx`,
		`${repoDir}/**/cache`,
		`${repoDir}/**/dist`,
		`${repoDir}/**/logs`,
		`${repoDir}/**/node_modules`,
		`${repoDir}/**/projects.err`,
		`${repoDir}/**/projects.bu`,
		`${repoDir}/**/tmp`,
		`${repoDir}/**/tools`,
		`${repoDir}/**/*.gif`,
		`${repoDir}/**/*.iso`,
		`${repoDir}/**/*.jpeg`,
		`${repoDir}/**/*.jpg`,
		`${repoDir}/**/*.pdf`,
		`${repoDir}/**/*.png`,
		`${repoDir}/**/*.tar.gz`,
		`${repoDir}/**/*.tar`,
		`${repoDir}/**/*.zip`,
	];
}

export function watch({ repoDir }) {
	const ignoredPathList = getIgnoredPathList({ repoDir });

	const absolutePathList = [];

	const listener = listen(repoDir, {
		ignored: ignoredPathList,
	})
		.on('add', (absolutePath) => {
			absolutePathList.push(absolutePath);
		})
		.on('error', (error) => {
			console.log(`Watcher error: ${error}`);
		})
		.on('ready', () => {
			format({ repoDir, relativeOrAbsoluteFilePathList: absolutePathList });
			listener.close();
		});
}

function cli() {
	const isCommandCalledFromCli = process.argv.length > 1;

	if (isCommandCalledFromCli) {
		const repoDir = process.argv[2];
		watch({ repoDir });
	}
}

cli();
