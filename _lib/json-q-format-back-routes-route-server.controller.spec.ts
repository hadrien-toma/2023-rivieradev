import { Test } from '@nestjs/testing';
import { JsonQFormatBackRoutesRouteServerController } from './json-q-format-back-routes-route-server.controller';

describe('JsonQFormatBackRoutesRouteServerController', () => {
  let controller: JsonQFormatBackRoutesRouteServerController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [],
      controllers: [JsonQFormatBackRoutesRouteServerController],
    }).compile();

    controller = module.get(JsonQFormatBackRoutesRouteServerController);
  });

  it('should be defined', () => {
    expect(controller).toBeTruthy();
  });
});
