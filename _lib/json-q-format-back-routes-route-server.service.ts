import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { firstValueFrom, of } from 'rxjs';

@Injectable()
export class JsonQFormatBackRoutesRouteServerService {
	async execute({ text, isValid }: { text: string; isValid: boolean }) {
		console.log(`projects/json/q-format/back/routes/route/server/src/lib/json-q-format-back-routes-route-server.service.ts`, { text });

		if (isValid) {
			const responseBody$ = of({ json: JSON.stringify(JSON.parse(text)) });
			return firstValueFrom(responseBody$);
		} else {
			const exception = {
				description: 'The text given is supposed to be valid',
				status: HttpStatus.BAD_REQUEST,
				title: 'Text is not JSON valid',
			};
			throw new HttpException(exception, HttpStatus.BAD_REQUEST);
		}
	}
}
