import { Body, Controller, Header, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { JsonQFormatBackRoutesRouteServerRequestBody } from './json-q-format-back-routes-route-server.request';
import { JsonQFormatBackRoutesRouteServerEventsResponseBody } from './json-q-format-back-routes-route-server.response';
import { JsonQFormatBackRoutesRouteServerService } from './json-q-format-back-routes-route-server.service';

@Controller('json-q-format-back-routes-route-server')
@ApiTags('json', 'q-format', 'back', 'routes', 'route', 'server')
export class JsonQFormatBackRoutesRouteServerController {
	constructor(private readonly service: JsonQFormatBackRoutesRouteServerService) {}

	@ApiOperation({ summary: 'This operation takes a text which is supposed to represent a JSON and try to format it' })
	@ApiQuery({
		description: 'True if the text as already been validated as a JSON',
		example: true,
		name: 'isValid',
		required: false,
		type: 'boolean',
	})
	@ApiResponse({
		status: StatusCodes.CREATED,
		description: 'The text has successfully been formatted as a JSON',
		type: JsonQFormatBackRoutesRouteServerEventsResponseBody,
	})
	@ApiResponse({
		status: StatusCodes.NOT_FOUND,
		description: getReasonPhrase(StatusCodes.NOT_FOUND),
	})
	@Header(
		'Authorization',
		`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c`,
	)
	@Post()
	async execute(
		@Query('isValid') isValid: boolean,
		@Body()
		{ text }: JsonQFormatBackRoutesRouteServerRequestBody,
	) {
		const responseBody: Promise<JsonQFormatBackRoutesRouteServerEventsResponseBody> = this.service.execute({ text, isValid });
		return responseBody;
	}
}
