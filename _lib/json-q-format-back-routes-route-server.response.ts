import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class JsonQFormatBackRoutesRouteServerEventsResponseBody {
	@ApiProperty({
		description: 'The formatted JSON',
		example: `{"conference": "Riviera DEV", "edition": "2023"}`,
		required: true,
		type: 'string',
	})
	@IsString()
	readonly json!: string;
}
