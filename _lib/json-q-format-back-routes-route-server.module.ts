import { Module } from '@nestjs/common';
import { JsonQFormatBackRoutesRouteServerController } from './json-q-format-back-routes-route-server.controller';
import { JsonQFormatBackRoutesRouteServerService } from './json-q-format-back-routes-route-server.service';

@Module({
	controllers: [JsonQFormatBackRoutesRouteServerController],
	providers: [JsonQFormatBackRoutesRouteServerService],
	exports: [],
})
export class JsonQFormatBackRoutesRouteServerModule {}
