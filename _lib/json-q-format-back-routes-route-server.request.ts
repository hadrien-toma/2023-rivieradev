import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class JsonQFormatBackRoutesRouteServerRequestBody {
	@ApiProperty({
		description: 'The text to format as a JSON',
		example: `{"edition": "2023", "conference": "Riviera DEV"}`,
		required: true,
		type: 'string',
	})
	@IsString()
	readonly text!: string;
}
